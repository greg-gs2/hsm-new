<?php


function homedemo_shortcode( $atts ) {
    $a = shortcode_atts( array(
    'label' => 'Book a Home Demo'
    ), $atts );
    return '<button class="button content-button" data-toggle="modal" data-target="#homedemo">' . esc_attr( $a['label'] ) . '</button>';
}
add_shortcode('homedemo', 'homedemo_shortcode');


function enquire_shortcode( $atts ) {
    $a = shortcode_atts( array(
    'label' => 'Enquire Online'
    ), $atts );
    return '<button class="button content-button" data-toggle="modal" data-target="#contactModal">' . esc_attr( $a['label'] ) . '</button>';
}
add_shortcode('enquire', 'enquire_shortcode');


function hire_shortcode( $atts ) {
    $a = shortcode_atts( array(
    'label' => 'Hire Quote'
    ), $atts );
    return '<button class="button content-button" data-toggle="modal" data-target="#hireModal">' . esc_attr( $a['label'] ) . '</button>';
}
add_shortcode('hire', 'hire_shortcode');


function button_shortcode( $atts ) {
     $a = shortcode_atts( array(
    'link' => '#',
    'label' => 'Click me'
    ), $atts );
    return '<a class="button" href="' . esc_url( $a['link'] ) . '">' . esc_attr( $a['label'] ) . '</a>';;
}
add_shortcode('button', 'button_shortcode');

function referral_shortcode( $atts ) {
    $a = shortcode_atts( array(
    'label' => 'Referral Someone'
    ), $atts );
    return '<button class="button content-button" data-toggle="modal" data-target="#referralModal">' . esc_attr( $a['label'] ) . '</button>';
}
add_shortcode('referral', 'referral_shortcode');
