<?php 

add_shortcode( 'activeplugins', function(){
	
	$active_plugins = get_option( 'active_plugins' );
	$plugins = "";
	if( count( $active_plugins ) > 0 ){
		$plugins = "<ul>";
		foreach ( $active_plugins as $plugin ) {
			$plugins .= "<li>" . $plugin . "</li>";
		}
		$plugins .= "</ul>";
	}
	return $plugins;
});


// deactivate_plugins( array( 'search-filter-pro/search-filter-pro.php' ) );
// activate_plugins( array( '/regenerate-thumbnails/regenerate-thumbnails.php' ) );


$url = $_SERVER["REQUEST_URI"];

// $usedWavs = strpos($url, '/used-wavs/');
$current_user = wp_get_current_user();
// // $usedWavs = strpos($url, 'used-wavs'); ADD More


// // Duplicate to use agian
// if (user_can( $current_user, 'administrator' )) {
//     activate_plugins( array( 'duplicate-post/duplicate-post.php', 'svg-support/svg-support.php', 'auto-image-attributes-from-filename-with-bulk-updater/iaff_image-attributes-from-filename.php', 'archived-post-status/archived-post-status.php' ) );
//     } else {
//     deactivate_plugins( array( 'duplicate-post/duplicate-post.php', 'svg-support/svg-support.php', 'auto-image-attributes-from-filename-with-bulk-updater/iaff_image-attributes-from-filename.php', 'archived-post-status/archived-post-status.php' ) );	
// }


add_filter( 'option_active_plugins', 'lg_disable_cart66_plugin' );

 function lg_disable_cart66_plugin($plugins){

      if(strpos($_SERVER['REQUEST_URI'], '/used-wavs/') === FALSE && !is_admin() ) {
         $key = array_search( 'search-filter-pro/search-filter-pro.php' , $plugins );
         if ( false !== $key ) unset( $plugins[$key] );
      }

     return $plugins;
 }


