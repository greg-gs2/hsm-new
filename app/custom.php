<?php

function buildSelect($postType)
{
	$return = '';

	$cpt = get_post_type_object($postType);

	$wavs = new WP_Query(array(
		'post_type'	 => $postType,
		'posts_per_page' => -1,
		'order' => 'ASC',
		'post_status' => array('publish')
	));

	if( $wavs->have_posts() ) :
		while( $wavs->have_posts() ) :
			$wavs->the_post();
			$link = get_the_permalink(get_the_ID());
			$selected = strpos($link, $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]) && !is_front_page() ? 'selected="selected"' : '';
			$return .= '<option value="' . $link . '"' . $selected . '>' . get_the_title() . '</option>';
		endwhile;
	else :
		$return .= '<option value="">' . $cpt->labels->not_found . '</option>';
	endif;
	wp_reset_postdata();

	return $return;
}

/* For custom meta boxes */
$prefix = 'gw_';

define('PREFIX', $prefix);

add_image_size('post_thumb', 350, 300, true);

/* Whether or not to show the featured image at the top of a page */
function showFeaturedImage($id)
{
	$exclude = array(274, 121);

	return !in_array($id, $exclude);
}

/* Checks if is displaying cars - not bespoke or used */
function isCarArchive()
{
	if( is_post_type_archive('car') )
	{
		if( !isset($_GET['type']) || ( $_GET['type'] == 'car' || $_GET['type'] == null) )
		{
			return true;
		}
	}

	if( is_tax('make') && ( !isset($_GET['type']) || $_GET['type'] == 'car' ) )
	{
		return true;
	}

	return false;
}

/* Checks if is displaying used cars */
function isUsedCarArchive()
{
	if ( is_post_type_archive('used_car') ) return true;

	if( is_tax('make') && isset($_GET['type']) && $_GET['type'] == 'used' ) return true;

	return false;
}

/* Checks if is displaying bespoke cars */
function isBespokeArchive()
{
	if( is_tax('make') && isset($_GET['type']) && $_GET['type'] == 'bespoke' ) return true;

	if ( is_post_type_archive('car') && isset($_GET['type']) && $_GET['type'] == 'bespoke' )
	{
		return true;
	}
}

/* Checks if it is the staff used car directory */
function isUsedCarDirectory()
{
	return is_page_template('page-used-directory.php');
}

/*--------------------------------------------------------------*
*						Used Cars 								*
*--------------------------------------------------------------*/

function showUsedWavVat($id)
{
	$showVat = get_post_meta($id, PREFIX . 'vat', true);
	return $showVat == 'on';
}

function getUsedFeatures($id)
{
	$features = get_post_meta($id, PREFIX . 'features', true);
	$features = str_replace("\n\r", "\n", $features);
	$features = explode("\n", $features);
	$features = array_filter($features);
	return $features;
}

function getUsedAdditional($id)
{
	$additional = get_post_meta($id, PREFIX . 'additional', true);
	$additional = str_replace("\n\r", "\n", $additional);
	$additional = explode("\n", $additional);
	$additional = array_filter($additional);
	return $additional;
}

function getUsedAttributes($id)
{	
	return array(
		'plate'			=> getUsedPlate($id),
		'engine' 		=> getUsedEngine($id),
		'transmission' 	=> getUsedTransmission($id),
		'fuel' 			=> getUsedFuel($id),
		'mileage' 		=> getUsedMileage($id),
		'colour' 		=> getUsedColour($id),
		'body-type'		=> getUsedBodyType($id),
		'passengers'	=> getUsedMaxPassengers($id),
		'wheelchairs'	=> getUsedMaxWheelchairs($id),
		'year'			=> getUsedYear($id)
	);
}

function getUsedSaving($id)
{
	$saving = get_post_meta($id, PREFIX . 'saving', true);
	if( $saving ) return '&pound;' . number_format($saving, 0);
	return null;
}

function getUsedPlate($id)
{
	return '\'' . get_post_meta($id, PREFIX . 'plate', true);
}

function getUsedNotes($id)
{
	return get_post_meta($id, PREFIX . 'notes', true);
}

function getUsedCommission($id)
{
	return get_post_meta($id, PREFIX . 'commission', true);
}

function getUsedBodyType($id)
{
	return get_post_meta($id, PREFIX . 'body-type', true);
}

function getUsedColour($id)
{
	return get_post_meta($id, PREFIX . 'colour', true);
}

function getUsedLocation($id)
{
	return get_post_meta($id, PREFIX . 'location', true);
}

function getUsedMileage($id)
{
	$mileage = get_post_meta($id, PREFIX . 'mileage', true);
	if( $mileage ) return number_format($mileage, 0);
	return null;
}

function getUsedEngine($id)
{
	return get_post_meta($id, PREFIX . 'engine', true);
}

function getUsedRegistration($id)
{
	return get_post_meta($id, PREFIX . 'Reg-number', true);
}

function getUsedStatus($id)
{
	return get_post_meta($id, PREFIX . 'status', true);
}

function getUsedTransmission($id)
{
	return get_post_meta($id, PREFIX . 'transmission', true);
}

function getUsedFuel($id)
{
	return get_post_meta($id, PREFIX . 'fuel', true);
}

function getUsedThumbs($id)
{
	return get_post_meta($id, PREFIX . 'gallery', true);
}

function getUsedPrice($id)
{
	$price = get_post_meta($id, PREFIX . 'price', true);
	if( $price ) return '&pound;' . number_format($price, 0);
	return null;
}

function getUsedMaxPassengers($id)
{
	return get_post_meta($id, PREFIX . 'seats', true);
}

function getUsedMaxWheelchairs($id)
{
	return get_post_meta($id, PREFIX . 'wheelchairs', true);
}

function getUsedYear($id)
{
	return get_post_meta($id, PREFIX . 'yeer', true);
}

function getUsedAge($id)
{
	return (int) date('Y') - getUsedYear($id);
}

function isExDemo($id)
{
	return get_post_meta($id, PREFIX . 'demo', true) == 'yes';
}

function getUsedPrepped($id)
{
	return get_post_meta($id, PREFIX . 'prepped', true) == 'yes' ? 'Yes' : 'No';
}

function isPurposeBuilt($id)
{
	return get_post_meta($id, PREFIX . 'purpose', true) == 'yes';
}

/*--------------------------------------------------------------*
*							Brochures							*
*--------------------------------------------------------------*/

function getBrochures()
{
	$return = array(
		''	=> 'None'
	);

	$brochures = new WP_Query(array(
		'post_type'		=> 'brochure',
		'posts_per_page'=> -1
	));

	if( $brochures->have_posts() ) :

		while( $brochures->have_posts() ) :

			$brochures->the_post();

			$return[get_the_ID()] = get_the_title();

		endwhile;

	endif;

	wp_reset_postdata();

	return $return;
}

/*--------------------------------------------------------------*
*							Misc 								*
*--------------------------------------------------------------*/

/* Function which filters the query on page load. Uses GET parameters which are attributes from the used car custom post type */
function meta_filter_posts( $query ) {

	if( isUsedCarDirectory() ) return;

	if( is_admin() || !$query->is_main_query() ) return $query;

	$attributes = getAttributesArray();

	$meta_query = array();
	$tax_query = array();

	if( isCarArchive() || isBespokeArchive() )
	{
		$value = isset($_GET['type']) && $_GET['type'] == 'bespoke' ? 'yes' : 'no';

		$meta_query[] = array(
			'key'		=> 'gw_bespoke',
			'value'		=> $value,
			'compare' 	=> '='
		);
	}

	/* If on a category page then show new cars by default. If type parameter is specified as used then show used cars */
	if( is_tax('make') )
	{
		if( isset( $_GET['type'] ) && $_GET['type'] == 'used' )
		{
			$query->set('post_type', 'used_car');
		}
		else
		{
			// Don't show used cars
			$query->set('post_type', 'car');
		}
	}

	/* Order by Price */
	if( isCarArchive() || isUsedCarArchive() || isBespokeArchive() )
	{
		$query->set('posts_per_page', -1);

		if( isUsedCarArchive() || isBespokeArchive() )
		{
			$query->set('meta_key', 'gw_price');
		}
		else
		{
			$query->set('meta_key', 'gw_weekly');
		}

		$query->set('orderby', 'meta_value_num');

		if( isset( $_GET['order'] ) && $_GET['order'] != null )
		{
			if( $_GET['order'] == 'DESC' )
			{
				$query->set('order', 'DESC');
			}

			if( $_GET['order'] == 'ASC' )
			{
				$query->set('order', 'ASC');
			}
		}
		else
		{
			$query->set('order'	, 'ASC');
		}
	}

	/* Number of posts per page */
	if( isset( $_GET['perpage'] ) && $_GET['perpage'] != null )
	{
		$query->set('posts_per_page', $_GET['perpage'] );
	}

	// foreach( $attributes as $attribute )
	// {
	// 	if( isset( $_GET[$attribute] ) && $_GET[$attribute] != null)
	// 	{	
	// 		if( $attribute == 'manufacturer' )
	// 		{
	// 			$tax_query[] = array(
	// 				'taxonomy'	=> 'make',
	// 				'field'		=> 'slug',
	// 				'terms'		=> $_GET['manufacturer']
	// 			);
	// 		}
	// 		else
	// 		{
	// 			$meta_query[] = array(
	// 				'key'		=> 'gw_' . $attribute,
	// 				'value'		=> $_GET[$attribute],
	// 				'compare'	=> '='
	// 			);
	// 		}
	// 	}
	// }

	/* Show ex-demo used cars */
	if( isset($_GET['demo'] ) && $_GET['demo'] )
	{
		$meta_query[] = array(
			'key'		=> 'gw_demo',
			'value'		=> 'on',
			'compare'	=> '='
		);
	}

	/* Show purpose built used cars */
	if( isset($_GET['purpose'] ) && $_GET['purpose'] )
	{
		$meta_query[] = array(
			'key'		=> 'gw_purpose',
			'value'		=> 'on',
			'compare'	=> '='
		);
	}

	/* Show standard used cars */
	if( isset($_GET['standard'] ) && $_GET['standard'] )
	{
		$meta_query[] = array(
			'key'		=> 'gw_standard',
			'value'		=> 'on',
			'compare'	=> '='
		);
	}

	/* Filter by price. Use weekly meta key for normal cars and full price key for used cars */
	if( isset($_GET['min']) && $_GET['min'] != null )
	{
		$max = isset($_GET['max']) && $_GET['max'] != null ? $_GET['max'] : getMaxPrice();

		if( isCarArchive() )
		{
			$key = 'gw_weekly';
		}
		else
		{
			$key = 'gw_price';
		}

		$meta_query[] = array(
			'key'	=> $key,
			'value'	=> array(
				intval( $_GET['min'] ),
				$max
			),
			'compare'	=> 'BETWEEN',
			'type'		=> 'UNSIGNED'
		);
	}

	// Age filter on used cars
	if( isset($_GET['max_age']) && $_GET['max_age'] != null )
	{
		$min = isset($_GET['min_age']) && $_GET['min_age'] != null ? $_GET['min_age'] : getMinAge();

		$max = $_GET['max_age'];

		$meta_query[] = array(
			'key'	=> 'gw_yeer',
			'value'	=> array(
				$min,
				$max
			),
			'compare'	=> 'BETWEEN',
			'type'		=> 'UNSIGNED'
		);
	}

	if( count($meta_query) )
	{
		$query->set( 'meta_query', $meta_query );
	}

	if( count($tax_query) )
	{
		$query->set( 'tax_query', $tax_query );
	}

	/* Offset by one on post archive pages so we can have a featured post at the top */
	if( is_archive() && ! ( isBespokeArchive() || isCarArchive() || isUsedCarArchive() ) )
	{
		if( get_query_var('paged') < 2 )
		{
			$query->set( 'offset', 1 );
		}
	}

	return $query;
}

add_filter( 'pre_get_posts', 'meta_filter_posts' );

/* An array of all of the attributes we would like to filter by. Name should correspond to the meta field name. Spits out select tags in filter template. */
function getAttributesArray()
{
	global $wp_query;

	$used = array(
		'Manufacturer'			=> 'manufacturer',
		'Model'					=> 'model',
		'Wheelchairs Spaces'	=> 'wheelchairs',
		'Max Passenger Seats'	=> 'seats',
		'Colour'				=> 'colour',
		'Fuel Type'	 			=> 'fuel',
		'Gearbox' 				=> 'transmission'
	);

	$new = array(
		'Make'		=> 'manufacturer',
		'Body Type' => 'body-type'
	);

	$directory = array(
		'Model'	=> 'model'
	);

	if( isUsedCarDirectory() ) return $directory;

	if( isUsedCarArchive() ) return $used;

	if( isCarArchive() || isBespokeArchive() ) return $new;
}

/* Gets the values from ALL CARS for all of the keys in the attributes array */
function getAllAttributes()
{
	$attributes = getAttributesArray();

	$return = buildReturnArray($attributes);

	$cars = getAllCars();

	if( $cars->have_posts() ) :

		while( $cars->have_posts() ) :

			$cars->the_post();

			foreach( $attributes as $attribute )
			{
				$meta = get_post_meta(get_the_ID(), 'gw_' . $attribute, true);

				if($meta)
				{
					$return[$attribute]['values'][$meta] = $meta;
				}
			}

		endwhile;

	endif;

	wp_reset_postdata();

	if( in_array('manufacturer', $attributes) )
	{
		$makes = get_terms('make');

		if( $makes )
		{
			foreach( $makes as $make )
			{
				$return['manufacturer']['values'][$make->slug] = $make->name;
			}
		}
	}

	return $return;
}


/* Gets the values from only the queried cars for all of the keys in the attributes array. These are used when filtering - we compare them with the values from all cars taken above and can grey out values which have no cars */
function getQueriedAttributes()
{
	$attributes = getAttributesArray();

	$return = buildReturnArray($attributes);

	global $wp_query;

	if( $wp_query->posts ) :

		foreach( $wp_query->posts as $post ) :

			setup_postdata( $post );

			foreach( $attributes as $attribute )
			{
				$meta = get_post_meta($post->ID, PREFIX . $attribute, true);

				if($meta)
				{
					$return[$attribute][$meta] = $meta;
				}
			}

		endforeach;

	endif;

	wp_reset_postdata();

	return $return;
}

/* Gets the maximum price of all used cars (for use with the price filter) */
function getMaxPrice()
{
	$max = 0;

	$cars = getAllCars();

	if( $cars->have_posts() ) :

		while( $cars->have_posts() ) :

			$cars->the_post();

			if( get_post_type() == 'car' )
			{
				if( isBespoke( get_the_ID() ) ) 
				{
					$price = get_post_meta(get_the_ID(), PREFIX . 'price', true);
				}
				else
				{
					$price = get_post_meta(get_the_ID(), PREFIX . 'weekly', true);
				}
			}

			if( get_post_type() == 'used_car' )
			{
				$price = get_post_meta(get_the_ID(), PREFIX . 'price', true);
			}

			$max = intval($price) > $max ? $price : $max;

		endwhile;

	endif;

	return $max;
}

function getMinAge()
{
	$min = 100000;

	$cars = new WP_Query(array(
		'post_type' => 'used_car',
		'posts_per_page' => -1
	));

	if( $cars->have_posts() ) :
		while( $cars->have_posts() ) :
			$cars->the_post();
			$min = getUsedYear(get_the_ID()) < $min ? getUsedYear(get_the_ID()) : $min;
		endwhile;
	endif;
	wp_reset_postdata();

	return $min;
}

function getAllCars()
{
	global $wp_query;

	$meta_query = array();
	$tax_query = array();

	if( isCarArchive() )
	{
		$postType = 'car';

		$meta_query[] = array(
			'key'		=> 'gw_bespoke',
			'value'		=> 'no',
			'compare'	=> '='
		);
	}

	if( is_post_type_archive() )
	{
		$postType = $wp_query->query_vars['post_type'];
	}

	if( is_tax() )
	{
		$tax_query[] = array(
			'taxonomy'	=> 'make',
			'field'		=> 'slug',
			'terms'		=> get_query_var('make')
		);

		if( isset($_GET['type']) && $_GET['type'] == 'used' )
		{
			$postType = 'used_car';
		}
		else
		{
			$postType = 'car';
		}
	}

	if( isUsedCarDirectory() )
	{
		$postType = 'used_car';
	}

	$args = array(
		'post_type'			=> $postType,
		'posts_per_page'	=> -1,
		'meta_query'		=> $meta_query,
		'tax_query'			=> $tax_query
	);

	$cars = new WP_Query($args);

	return $cars;

	wp_reset_postdata();
}

add_action( 'wp_ajax_buildFormHtml', 'buildFormHtml' );
add_action( 'wp_ajax_nopriv_buildFormHtml', 'buildFormHtml' );

function buildFormHtml($params)
{
	$all 		= getAllAttributes();
	$current	= getQueriedAttributes();

	$html = '';

	if( $all ) :

		foreach( $all as $name => $atts ) :

			$html .= '<div class="form-group"><label>' . ucwords($name) . '</label><select name="' . $name . '" class="form-control"><option value="">Any</option>';

			if($atts) :

				foreach( $atts as $att ) :

					$selected = isset($_GET[$name]) && $_GET[$name] == $att ? 'selected' : '';
					$disabled = !in_array($att, $current[$name]) ? 'disabled' : '';

					$html .= '<option value="' . $att . '"' . $disabled . ' ' . $selected . '>' . $att . '</option>';
		
				endforeach;

			endif;

			$html .= '</select></div>';
	
		endforeach;
	endif;

	echo $html;

	die();
}

/* Make Share Link for Twitter */
function makeTwitterShareUrl($id)
{
	return 'http://twitter.com/share?text=' . urlencode(get_the_title($id)) . '&url=' . urlencode(get_permalink($id));
}

/* Make Share Link for Facebook */
function makeFacebookShareUrl($id)
{
	return 'https://www.facebook.com/sharer.php?u=' . urlencode(get_permalink($id));
}

function getTemplatePart()
{
	ob_start();
	get_template_part('templates/tabs/' . $_GET['template']);
	echo ob_get_clean();
	die();
}

add_action("wp_ajax_getTemplatePart", "getTemplatePart");
add_action("wp_ajax_nopriv_getTemplatePart", "getTemplatePart");

function isUsedPurpose()
{
	if( !isUsedCarArchive() ) return false;

	if( isset( $_GET['purpose'] ) && $_GET['purpose'] == 'yes' ) return true;

	return false;
}


function isUsedStandard()
{
	if( !isUsedCarArchive() ) return false;

	if( isset( $_GET['standard'] ) && $_GET['standard'] == 'yes' ) return true;

	return false;	
}


function isUsedDemo()
{
	if( !isUsedCarArchive() ) return false;

	if( isset( $_GET['demo'] ) && $_GET['demo'] == 'yes' ) return true;

	return false;	
}

// Function to add Response tap to posts and pages
  function tap_shortcode() {
    return '<span class="rTapNumber4345" onclick="rTapClickToCall(4345)">0800 916 3018</span>';
}
add_shortcode('tap', 'tap_shortcode');

// Function to add Response tap to posts and pages
  function dreamtap_shortcode() {
    return '<span class="rTapNumber435681" onclick="rTapClickToCall(435681)">0800 088 5592</span>';
}
add_shortcode('dreamtap', 'dreamtap_shortcode');

// Function to add Response tap to posts and pages
  function hiretap_shortcode() {
    return '<span class="rTapNumber4349" onclick="rTapClickToCall(4349)">0800 916 0015</span>';
}
add_shortcode('hiretap', 'hiretap_shortcode');

function ajaxFindMakes()
{
	global $post;

	$type = $_GET['type'];
	$cpt = $_GET['cpt'];

	$meta_query = array();
	$makes = array();

	$return = '<option value="">Please select</option>';

	switch($type)
	{
		case "bespoke" :

			if( $cpt == 'car' )
			{
				$key 	= 'gw_bespoke';
				$value 	= 'yes';
			}

			if( $cpt == 'used_car' )
			{
				$key 	= 'gw_purpose';
				$value 	= 'on';
			}

			$meta_query[] = array(
				'key'		=> $key,
				'value'		=> $value,
				'compare'	=> '='
			);

			break;

		case "standard" :

			$meta_query[] = array(
				'key'		=> 'gw_standard',
				'value'		=> 'on',
				'compare'	=> '='
			);

			break;

		case "demo" :

			$meta_query[] = array(
				'key'		=> 'gw_demo',
				'value'		=> 'on',
				'compare'	=> '='
			);

			break;
	}

	$cars = new WP_Query(array(
		'post_type'			=> $cpt,
		'posts_per_page'	=> -1,
		'meta_query'		=> $meta_query
	));

	if( $cars->have_posts() ) :

		while( $cars->have_posts() ) :

			$cars->the_post();

			$terms = get_the_terms(get_the_ID(), 'make');

			if( $terms )
			{
				foreach( $terms as $term )
				{
					$makes[$term->slug] = $term->name;
				}
			}

		endwhile;

	endif;

	wp_reset_postdata();

	if( count($makes) )
	{
		foreach($makes as $slug => $name)
		{
			$return .= '<option value="' . $slug . '">' . $name . '</option>';
		}
	}

	else
	{
		$return .= '<option value="">Sorry, no taxis found</option>';
	}

	echo $return;

	die();
}

add_action("wp_ajax_ajaxFindMakes", "ajaxFindMakes");
add_action("wp_ajax_nopriv_ajaxFindMakes", "ajaxFindMakes");

function ajaxFindUsedVehicles()
{
	global $post;

	$models = array();

	$return = '<option value="">Select model</option>';

	$vehicles = new WP_Query(array(
		'post_type' => 'used_car',
		'post_status' => 'publish',
		'make' => $_GET['make'],
		'posts_per_page' => -1
	));

	if( $vehicles->have_posts() ) :

		while( $vehicles->have_posts() ) :

			$vehicles->the_post();

			$model = get_post_meta($post->ID, PREFIX . 'model', true);

			if( $model )
			{
				$models[] = $model;
			}

		endwhile;

		$models = array_unique($models);

		foreach( $models as $model )
		{
			$return .= '<option value="' . $model . '">' . $model . '</option>';
		}

	else :

		$return .= '<option value="">Sorry, no used WAVs found</option>';

	endif;

	echo $return;

	die();
}

add_action("wp_ajax_ajaxFindUsedVehicles", "ajaxFindUsedVehicles");
add_action("wp_ajax_nopriv_ajaxFindUsedVehicles", "ajaxFindUsedVehicles");

function getUsedMakes()
{
	$makes = array();
	$return = '';

	$cars = new WP_Query(array(
		'post_type' => 'used_car',
		'post_status' => 'publish',
		'posts_per_page' => -1
	));

	if( $cars->have_posts() ) :

		while( $cars->have_posts() ) :

			$cars->the_post();

			$terms = get_the_terms(get_the_ID(), 'make');

			if( $terms )
			{
				foreach( $terms as $term )
				{
					$makes[$term->slug] = $term->name;
				}
			}

		endwhile;

	endif;

	wp_reset_postdata();

	if( count($makes) )
	{
		foreach($makes as $slug => $name)
		{
			$return .= '<option value="' . $slug . '">' . $name . '</option>';
		}
	}

	return $return;
}

/**
 * Checks if we are viewing the minibus
 * @param  int  $id post ID
 * @return boolean
 */
function isMinibus($id)
{
	return $id == 784;
}

function isHirePage()
{
	global $post;

	return $post->ID == 150 ||  $post->ID == 153;
	// return true;
}

function getCarNearlyNewPrice($id)
{
	$price = get_post_meta($id, PREFIX . 'nearly_new_price', true);
	if( $price ) return '&pound;' . number_format($price, 0);
	return null;
}

function getMakes()
{
	$dbhost = CARS_DB_HOST;
	$dbuser = CARS_DB_USER;
	$dbpass = CARS_DB_PASS;
	$dbname = CARS_DB_NAME;

	$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
   
	$sql = "SELECT make, model, makeName, modelName FROM cars";

	//$result = $conn->query($sql);

	while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){

		$make = $row['make'];
		$makeName = $row['makeName'];
		$model = $row['model'];
		$modelName = $row['modelName'];


		$makes[$make] = $makeName;
	}

	return $makes;

}



function getModels()
{
	$dbhost = CARS_DB_HOST;
	$dbuser = CARS_DB_USER;
	$dbpass = CARS_DB_PASS;
	$dbname = CARS_DB_NAME;

	$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
   
	$sql = "SELECT make, model, makeName, modelName FROM cars";

	//$result = $conn->query($sql);

	while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){

		$make = $row['make'];
		$makeName = $row['makeName'];
		$model = $row['model'];
		$modelName = $row['modelName'];


		$models[$model] = $modelName;
	}

	return $models;

}

function checkbox_checked($value)
{
    return $value == 'on' ? 'true' : 'false';
}

add_filter('the_time', 'modify_date_format');
function modify_date_format(){
    $month_names = array(1=>'Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember');
    return get_the_time('j').'. '.$month_names[get_the_time('n')].' '.get_the_time('Y');
}
