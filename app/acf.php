<?php

add_theme_support( 'post-formats', array( 'aside', 'gallery', 'quote' ) );

/**
 * ACF Feilds
 */
 if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Tracking Codes',
        'menu_title'    => 'Tracking Codes',
        'parent_slug'   => 'theme-general-settings',
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    => 'Promo Boxes',
        'menu_title'    => 'Promo Boxes',
        'parent_slug'   => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => '404 Page',
        'menu_title'    => '404 Page',
        'parent_slug'   => 'theme-general-settings',
    ));
}

// add_action('pre_get_posts', 'skip_posts_wordpress_archive_cpt_func', 50);
// function skip_posts_wordpress_archive_cpt_func($query){
 
//     if(!is_admin() && $query->is_archive()){
//         $query->set('offset', 0);
//     }
// }

// add menu image
add_filter( 'wp_nav_menu_objects', function( $items ) {
      foreach ( $items as $item ) {

         $icon = get_field('menu_image', $item);


        if ($icon) {
           $item->title = '<span class="menu-images-container"><img class="menu-images" src="'.$icon.'" alt="'.$item->title.'" /></span> <span class="menu-text">' . $item->title . '</span>';
        }
    }
    return $items;
});


/**
 * Paged Blog
 */

function numeric_posts_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
 
    echo '</ul></div>' . "\n";
 
}


// list children of pages

function wpb_list_child_pages()
{
    global $post;

    if ( is_page() && $post->post_parent )
    {
        $childpages = wp_list_pages( 'sort_column=post_date&title_li=&child_of=' . $post->post_parent . '&echo=0' );
    }
    else
    {
        $childpages = wp_list_pages( 'sort_column=post_date&title_li=&child_of=' . $post->ID . '&echo=0' );
    }

    if ( $childpages )
    {
        $string = '<ul class="submenu-page">' . $childpages . '</ul>';
    }

    return $string;
}

add_shortcode('wpb_childpages', 'wpb_list_child_pages');


/**
 * Hide menu items from the admin menu
 */
add_action('admin_menu', function() {
    // List of users that don't have pages removed
    $admins = [
        'greg.gibson', 'ggibson',
    ];

    $current_user = wp_get_current_user();

    if (!in_array($current_user->user_login, $admins)) {
        remove_menu_page('edit.php?post_type=acf-field-group');
    }
}, PHP_INT_MAX);



/**
 * Remove Editor
 */
// add_action( 'init', function() {
//     remove_post_type_support( 'page', 'editor' );
// }, 99);

// /**
//  * Remove Editor
//  */
// add_action( 'init', function() {
//     remove_post_type_support( 'post', 'editor' );
// }, 99);

add_filter("use_block_editor_for_post_type", "disable_gutenberg_editor");
function disable_gutenberg_editor()
{
return false;
}

/**
 * Gravity Form UK format
 */

add_filter( 'gform_phone_formats', 'uk_phone_format' );
function uk_phone_format( $phone_formats ) {
    $phone_formats['uk'] = array(
        'label'       => 'UK',
        'mask'        => false,
        'regex'       => '/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/',
        'instruction' => false,
    );
 
    return $phone_formats;
}

/**
 * Gravity Wiz // Gravity Forms // Capitalize Submitted Data
 *
 * Capitializes all words in Single Line Text, Paragraph Text, Address and Name fields.
 *
 */
add_action( 'gform_pre_submission', 'gw_capitalize_submitted_data' );
function gw_capitalize_submitted_data( $form ) {

    $applicable_input_types = array( 'address', 'text', 'name' );

    foreach( $form['fields'] as $field ) {

        $input_type = GFFormsModel::get_input_type( $field );

        if( ! in_array( $input_type, $applicable_input_types ) ) {
            continue;
        }

        if( isset( $field['inputs'] ) && is_array( $field['inputs'] ) ) {
            foreach( $field['inputs'] as $input ) {
                $input_key = sprintf( 'input_%s', str_replace( '.', '_', $input['id'] ) );
                $_POST[ $input_key ] = ucwords( rgpost( $input_key ) );
            }
        } else {
            $input_key = sprintf( 'input_%s', $field['id'] );
            $_POST[ $input_key ] = ucwords( rgpost( $input_key ) );
        }

    }

    return $form;
}

/**
 * Disable the emoji's
 */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}
