// import external dependencies
import 'jquery';
import 'slick-carousel/slick/slick.min.js';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());


    document.addEventListener('DOMContentLoaded',
        function() {
            var div, n,
                v = document.getElementsByClassName('youtube-player');
            for (n = 0; n < v.length; n++) {
                div = document.createElement('div');
                div.setAttribute('data-id', v[n].dataset.id);
                div.innerHTML = labnolThumb(v[n].dataset.id);
                div.onclick = labnolIframe;
                v[n].appendChild(div);
            }
        });

function labnolThumb(id) {
        var thumb = '<img src="https://i.ytimg.com/vi/ID/sddefault.jpg">',
            play = '<div class="play"><svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" class="svg-inline--fa fa-youtube fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg></div>';
        return thumb.replace('ID', id) + play;
    }

function labnolIframe() {
        var iframe = document.createElement('iframe');
        var embed = 'https://www.youtube.com/embed/ID/?rel=0';
        iframe.setAttribute('src', embed.replace('ID', this.dataset.id));
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('allowfullscreen', '1');
        this.parentNode.replaceChild(iframe, this);
}

 //stop youtube video on modal close
$('.modal').on('hidden.bs.modal', function () {
    var iframVideo = $('.modal').find('iframe');
    $(iframVideo).attr('src', $(iframVideo).attr('src'));
});
