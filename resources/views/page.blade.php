@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="row">
    	<div class="col-sm-12 col-lg-9">
        @include('partials.page-header')
    		@include('partials.page-builder')
        @include('partials.builder-elements.footer-cta')
    	</div>
    	<div class="col-sm-12 col-lg-3 sidebar">
	  		@include('partials.sidebar')
		</div>
	</div>
  @endwhile
@endsection
