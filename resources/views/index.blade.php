@extends('layouts.app')

@section('content')
  {{--@include('partials.page-header')--}}
  <div class="row">
    <div class="page-header">
      <h1>{!! App::title() !!}</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-lg-9">
        @if (!have_posts())
          <div class="alert alert-warning">
            {{ __('Sorry, no results were found.', 'sage') }}
          </div>
          {!! get_search_form(false) !!}
        @endif

        <?php $featured = new WP_Query(array(
            'posts_per_page'  => 1,
            'offset' => 0,
            'post_type'     => 'post',
          )); ?>

          @include('partials.builder-elements.news.featured')

        <?php $allposts = new WP_Query(array(
          'post_type'     => 'post',
          'posts_per_page'  => 10,
          'offset' => 1,
        )); ?>

        @while ($allposts->have_posts()) @php $allposts->the_post() @endphp
          @include('partials.content-'.get_post_type())
        @endwhile

        {{--{!! get_the_posts_navigation() !!}--}}
        <?php numeric_posts_nav(); ?>
    </div>
    <aside class="col-sm-12 col-lg-3 sidebar sidebar-promos">
       @include('partials.builder-elements.promo-boxes')
       @include('partials.sidebar')
    </aside>
  </div>
@endsection
