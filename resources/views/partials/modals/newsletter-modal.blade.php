<!-- Modal -->
<div class="modal fade" id="newsmodal" tabindex="-1" role="dialog" aria-labelledby="newsmodalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
      </button>
      <div class="modal-body pop-up-form">
        <?php if( have_rows('logos', 'option') ): ?>
          <?php while( have_rows('logos', 'option') ) : the_row(); ?>
            <img src="<?php the_sub_field('logo_alternative', 'option'); ?>" alt="{{ get_bloginfo('name', 'display') }}" style="max-width: 250px; margin-bottom:20px;">
          <?php endwhile; ?>
        <?php endif; ?>
        <h2>Newsletter Signup</h2>
       <!--  <?php //get_template_part('templates/forms/enquire_online'); ?> -->
        <?php if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) { gravity_form(3, false, false, false, null, true); } ?>
      </div>
    </div>
  </div>
</div>