<div class="col-12 col-md-4 col-lg-3">
  <article @php post_class() @endphp>
    <div class="container search-post-cont">
      <div class="row d-lg-flex">
        <?php if ( has_post_thumbnail()) : ?>
            <div class="col-12 search-post-featured-image align-self-center">
              <?php the_post_thumbnail(); ?>
            </div>
        <?php endif; ?>
            <div class="col-12 align-self-center">
              <header>
                <h4 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h4>
              </header>
              <a href="{{ get_permalink() }}" class="button width-100">Find Out More</a>
          </div>
      </div>
    </div>
  </article>
</div>
