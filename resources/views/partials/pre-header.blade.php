<div class="container pre-header hide-mobile">
  <div class="row">
    <div class="col-sm-12 col-lg-6">
      <button class="button" data-toggle="modal" data-target="#contactModal">Enquire Online</button>
      <?php if( get_field('phone_number', 'options') ): ?>
        <div class="header-phone">
        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="phone" class="svg-inline--fa fa-phone fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg> <span class="rTapNumber4282" onclick="rTapClickToCall(4282)"><span class="hide-mobile"><?php the_field('phone_number', 'options'); ?></span></span></div>
      <?php endif; ?>
    </div>
    <div class="col-sm-12 col-lg-6 header-right">

        <!-- Button trigger modal -->
        <a class="search-button-header hide-mobile" data-toggle="modal" data-target="#searchModal">
          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg>
        </a>

        <!-- Countries -->
        <?php if( have_rows('flags', 'option') ): ?>
          <?php while( have_rows('flags', 'option') ) : the_row(); ?>
              <div class="flag-dropdown">
                  <span class="country"><img src="<?php the_sub_field('current_flag', 'option'); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>" style="max-width:30px;"/></span> <img src="https://www.alliedmobility.com/wp-content/themes/ebay-theme/assets/img/chevron-down-solid.svg" class="flag-indicator" style="width: 15px;"/>
                <div class="dropdown-content">
                  <?php if( have_rows('flag_repeater', 'option') ): ?>
                    <?php while( have_rows('flag_repeater', 'option') ): the_row(); ?>
                      <a href="<?php the_sub_field('link'); ?>"><img src="<?php the_sub_field('flag'); ?>" alt="Allied Mobility <?php the_sub_field('country'); ?>" style="max-width:20px;" /> <?php the_sub_field('country'); ?></a>
                    <?php endwhile; ?>
                  <?php endif; ?>
                </div>
              </div>                       
          <?php endwhile; ?>
        <?php endif; ?>
        <!-- Countries -->

    </div>
  </div>
</div>
