<header class="banner<?php if( get_field('full_width_header') ): ?> full-header<?php endif; ?>">
  <div class="container header-container">
    <div class="row">
      <div class="col-4 col-lg-2 align-self-center no-padding">
          <a class="brand logo" href="{{ home_url('/') }}">
            <?php if( have_rows('logos', 'option') ): ?>
              <?php while( have_rows('logos', 'option') ) : the_row(); ?>
                <?php if( get_field('full_width_header') or is_singular('car')): ?>
                    <img src="<?php the_sub_field('logo_alternative', 'option'); ?>" alt="{{ get_bloginfo('name', 'display') }}" style="width:100%;">
                   <?php else: ?>
                    <img src="<?php the_sub_field('logo', 'option'); ?>" alt="{{ get_bloginfo('name', 'display') }}" style="width:100%;">
                <?php endif; ?>
              <?php endwhile; ?>
            <?php endif; ?>
          </a>
      </div>
      <div class="col-4 col-lg-8 align-right align-self-center hide-mobile">

            <nav class="navbar navbar-expand-lg hide-mobile" role="navigation">
              <div class="collapse navbar-collapse mx-auto"  id="navbarSupportedContent">
                @if (has_nav_menu('primary_navigation'))
                  {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'depth' => 4, 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'navbar-nav nav ml-auto']) !!}
                @endif
              </div>
            </nav>
         
      </div>
      <div class="col-8 col-lg-2 align-right align-self-center header-icons">

            <?php if( get_field('phone_number', 'options') ): ?>
              <a href="tel:<?php the_field('phone_number', 'options'); ?>"><svg aria-hidden="true" style="max-width: 30px;max-height:30px; color: #fff;" focusable="false" data-prefix="fas" data-icon="phone" class="svg-inline--fa fa-phone fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg></a>
            <?php endif; ?> 

            <a data-toggle="modal" data-target="#contactModal" ><svg aria-hidden="true" style="max-width: 30px;max-height:30px; color: #fff;" focusable="false" data-prefix="fas" data-icon="envelope" class="svg-inline--fa fa-envelope fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path></svg></a>

            <a class="navbar-toggler mob-nav-con hide-desktop" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false"
                    aria-label="Toggle navigation">
              <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" class="svg-inline--fa fa-bars fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg>
            </a>
      
        </div>
            
    </div>
  </div>
  
          <div class="col-sm-12 mob-nav col-lg-12 hide-desktop">
              <nav class="navbar navbar-expand-lg" role="navigation">
                <div class="collapse navbar-collapse nav-primary ml-auto justify-content-md-center"  id="navbarSupportedContent">
                  @if (has_nav_menu('primary_navigation'))
                    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'depth' => 4, 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'navbar-nav nav ml-auto']) !!}
                  @endif

                  <!-- <div class="mobile-nav-footer">
                    <button class="button" data-toggle="modal" data-target="#contactModal">Enquire Online</button>
                    <div class="header-phone">
                      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="phone" class="svg-inline--fa fa-phone fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg> <span class="rTapNumber4282" onclick="rTapClickToCall(4282)"><?php the_field('phone_number', 'options'); ?></span>
                    </div>
                  </div> -->

                </div>
              </nav>
          </div>
</header>
