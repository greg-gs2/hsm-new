<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- hreflang Tag -->
    <?php while( have_rows('alternative_lang_urls') ): the_row(); ?>
    <link rel="alternate" href="<?php the_sub_field('url'); ?>" hreflang="<?php the_sub_field('language_tag'); ?>" />
    <?php endwhile; ?>

    <!-- Category hreflang Tag -->
    <?php if (is_category('Allied Mobility News') ): ?>
        <?php while( have_rows('alternative_lang_urls', 'category_25') ): the_row(); ?>
            <link rel="alternate" href="<?php the_sub_field('url', 'category_25'); ?>" hreflang="<?php the_sub_field('language_tag', 'category_25'); ?>" />
        <?php endwhile; ?>
    <?php endif; ?>

  @php wp_head() @endphp

  <?php if( get_field('header_tracking_codes','options') ): ?>
      <?php the_field('header_tracking_codes', 'options'); ?>
  <?php endif; ?>

<?php if( have_rows('intro_section') ): ?>
  <?php while ( have_rows('intro_section') ) : the_row(); ?>
    <style>
    .mini-cont {
      background-image: url(<?php the_sub_field('header_image'); ?>);
    }
    @media only screen and (max-width: 768px) {
      .mini-cont {
        background-image: url(<?php the_sub_field('header_image_mobile'); ?>);
      }
    }
    </style>
  <?php endwhile;?>
<?php endif; ?>

</head>
