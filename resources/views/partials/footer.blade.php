<footer class="content-info footer container">
  <div class="container footer-widget-area">
    <div class="row">
      <div class="col-sm-12 col-md-3">
        <?php if( have_rows('logos', 'option') ): ?>
          <?php while( have_rows('logos', 'option') ) : the_row(); ?>
            <img src="<?php the_sub_field('logo', 'option'); ?>" alt="{{ get_bloginfo('name', 'display') }}" style="width:100%; margin-bottom: 20px;">
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
      <div class="col-sm-12 col-md-3">
        @php dynamic_sidebar('sidebar-footer-1') @endphp
      </div>
      <div class="col-sm-12 col-md-3">
        @php dynamic_sidebar('sidebar-footer-2') @endphp
      </div>
      <div class="col-sm-12 col-md-3">
        <h3>Customer Service Number</h3>
        <?php if( get_field('phone_number', 'options') ): ?>
         <span class="rTapNumber4282" onclick="rTapClickToCall(4282)"><?php the_field('phone_number', 'options'); ?></span>
        <?php endif; ?>
      </div>
    </div>
    <div class="row footer-logos">
      <div class="col-12">
         <?php if( have_rows('accreditation_logos', 'options') ): ?>
             <?php while( have_rows('accreditation_logos', 'options') ) : the_row(); ?>
                  <img src="<?php the_sub_field('images', 'options'); ?>" />
              <?php endwhile; ?>
          <?php endif; ?>
      </div>
    </div>
  </div>
</footer>
<div class="footer-copyright">
    <div class="container">
       <div class="row">
          <div class="col-sm-12 col-md-8 footer-left">
            @php dynamic_sidebar('sidebar-footer-copyright') @endphp
          </div>
          <div class="col-sm-12 col-md-4 footer-right">
             @include('partials.builder-elements.social')
          </div>
          
        </div>
      
    </div>
</div>

@include('partials.modals.newsletter-modal')
@include('partials.modals.contact-modal')
@include('partials.modals.search-modal')

<?php if( get_field('footer_tracking_codes','options') ): ?>
    <?php the_field('footer_tracking_codes', 'options'); ?>
<?php endif; ?>