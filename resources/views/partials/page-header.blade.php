<div class="page-header">
  <h1>{!! App::title() !!}</h1>
  	<?php if ( has_post_thumbnail()) : ?>
	    <?php if( is_search() ): ?>
	    <?php else: ?>
	    	<div class="page-header-img">
					<?php the_post_thumbnail('medium_large'); ?>
				</div>
			<?php endif; ?>
	<?php endif; ?>
</div>
