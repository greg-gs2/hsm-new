@php dynamic_sidebar('sidebar-primary') @endphp
<?php if( $children = wpb_list_child_pages() ) : ?>

		<section class="widget">
			<div class="widget-inner">
				<h3>Sub Navigation <!-- - <?php //echo get_the_title( $post->post_parent ); ?> --></h3>
				<?= $children; ?>
			</div>
		</section>

<?php endif; ?>
<!-- <div class="sidebar-promos">
{{--@include('partials.builder-elements.promo-boxes')--}}
</div> -->