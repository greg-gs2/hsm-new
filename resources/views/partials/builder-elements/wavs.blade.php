<?php $wavs = new WP_Query(array(
      'posts_per_page'  => -1,
      'post_type'     => 'car',
      'post__not_in' => array(27431),
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'post_status' => 'publish',
    )); ?>

     <?php if( $wavs->have_posts() ) : ?>
      <div class="more-wavs wavs-page">
        <div class="row">
          <?php while( $wavs->have_posts() ) : ?> 
                <?php $wavs->the_post(); ?>
            
              
              <div class="col-12 col-md-4">
                <a href="{{ get_permalink() }}">
                    <h2 class="wav-heading">{!! get_the_title() !!}</h2>
                    <div class="more-wavs-img">
                      <?php the_post_thumbnail(); ?>
                    </div>
                </a>
                <div class="more-wav-table
">
                  <div class="row">
                        <div class="col-8 wav-left">Advance Payment From only:</div>
                        <div class="col-4 wav-right"><?php the_field('advance_payment'); ?></div>
                   </div>
                  <div class="row">
                        <div class="col-8 wav-left">Cash Price From only:</div>
                        <div class="col-4 wav-right"><?php the_field('cash_price'); ?></div>
                  </div>
                </div>
                  <a href="{{ get_permalink() }}" class="button width-100 button-green">More Details</a>
                  <a href="{{ get_permalink() }}" class="button width-100">Book a Home Demo</a>
              </div>
            <?php endwhile; ?>
          </div>
    </div>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>