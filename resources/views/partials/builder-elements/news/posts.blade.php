<?php if( $news->have_posts() ) : ?>
    <div class="container blog-feed">
      <div class="row">
                    
        <?php while( $news->have_posts() ) : ?> 
          <?php $news->the_post();  global $post; ?>
            <div class="col-sm-12 col-lg-6">
              
              <article @php post_class() @endphp>
                 <div class="row">
                      <?php if ( has_post_thumbnail()) : ?>
                          <div class="col-sm-12 col-lg-5 post-featured-image">
                            <?php the_post_thumbnail('thumbnail'); ?>
                          </div>
                      <?php endif; ?>
                          <div class="col-sm-12 col-lg-7">
                            <header>
                              <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
                              <p class="post-meta">@include('partials/entry-meta')</p>
                            </header>
                            <div class="entry-summary">
                              @php the_excerpt() @endphp
                            </div>
                        </div>
                  </div>
                    <a href="{{ get_permalink() }}" class="button">Read More</a>
                </article>
            </div>
        <?php endwhile; ?>

      </div>
    </div>  

    <?php endif; ?>
   <?php wp_reset_postdata(); ?>