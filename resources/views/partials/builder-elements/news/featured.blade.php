<?php if( $featured->have_posts() ) : ?>
      <div class="row">
                    
        <?php while( $featured->have_posts() ) : ?> 
          <?php $featured->the_post();  global $post; ?>
            <div class="col-12 featured-post">
              
              <article @php post_class() @endphp>
                
                      <?php if( get_field('blog_featured_image') ): ?>
                         <picture>
                          <source media="(max-width: 630px)" srcset="<?php the_field('blog_featured_image_mobile'); ?>">
                          <img src="<?php the_field('blog_featured_image'); ?>" alt="<?php the_title(); ?>" class="img-rounded mb20">
                        </picture>
                       <?php else : ?> 
                            <?php the_post_thumbnail(); ?>
                      <?php endif; ?>
                      
                            <header>
                              <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
                              <p class="post-meta">@include('partials/entry-meta')</p>
                            </header>
                            <div class="entry-summary">
                              @php the_excerpt() @endphp
                            </div>
                    
                 
                    <a href="{{ get_permalink() }}" class="button">Read More</a>
                </article>
            </div>
        <?php endwhile; ?>

      </div>

    <?php endif; ?>
   <?php wp_reset_postdata(); ?>