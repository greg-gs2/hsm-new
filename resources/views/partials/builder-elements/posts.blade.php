
<div class="row post-tabs">
  <div class="col-12">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-news-tab" data-toggle="tab" href="#nav-news" role="tab" aria-controls="nav-news" aria-selected="true">Latest Allied Mobility News</a>
        <a class="nav-item nav-link" id="nav-dis-news-tab" data-toggle="tab" href="#nav-dis-news" role="tab" aria-controls="nav-dis-news" aria-selected="false">Latest Disability News</a>
      </div>
    </nav>
  </div>
</div>
<div class="post-section">
  <div class="row">
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-news" role="tabpanel" aria-labelledby="nav-news-tab">
        <?php $news = new WP_Query(array(
            'posts_per_page'  => 2,
            'post_type'     => 'post',
            'category__in'    => array(25)
          )); ?>
          @include('partials.builder-elements.news.posts')

          <div class="more-news"><a href="{{ get_permalink() }}" class="button width-100">More Allied Mobility News</a></div>
        </div>
        <div class="tab-pane fade" id="nav-dis-news" role="tabpanel" aria-labelledby="nav-dis-news-tab">
          <?php $news = new WP_Query(array(
            'posts_per_page'  => 2,
            'post_type'     => 'post',
            'category__in'    => array(203)
          )); ?>
          @include('partials.builder-elements.news.posts')

          <div class="more-news"><a href="{{ get_permalink() }}" class="button width-100">More Disability News</a></div>
        </div>
      </div>
  </div>

  <div class="row post-section-social">
    <div class="col-12">
      <div class="social-latest">
        <h2>Allied Mobility Social</h2>
        @include('partials.builder-elements.social')
      </div>
    </div>
  </div>
</div>
