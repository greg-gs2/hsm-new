<div class="container">
	<div class="row">
	<?php while(has_sub_field('block')): ?>
			<div class="col-12 col-md-3">
				<div class="home-boxes page-boxes">
					<img class="button-img" src="<?php the_sub_field('image'); ?>">
					<?php the_sub_field('content'); ?>
				</div>
			</div>
	<?php endwhile; ?>
	</div>
</div>


