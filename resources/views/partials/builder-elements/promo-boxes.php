
	<div class="container">
		<div class="row">
		<?php while(has_sub_field('promo_boxes', 'options')): ?>
				<div class="col-6 col-md-3">
					<div class="home-boxes">
						<a href="<?php the_sub_field('link', 'options'); ?>" <?php if( get_sub_field('external_link', 'options') == 'true' ):?> target="_blank" <?php endif;?>>
							<?php the_sub_field('title', 'options'); ?>
							
							<img class="button-img" src="<?php the_sub_field('image', 'options'); ?>" alt="<?php the_sub_field('title', 'options'); ?>">
							<p><?php the_sub_field('sub_title', 'options'); ?></p>
							<a href="<?php the_sub_field('link', 'options'); ?>" class="button">Discover Range</a>
						</a>
					</div>
				</div>
		<?php endwhile; ?>
		</div>
	</div>


