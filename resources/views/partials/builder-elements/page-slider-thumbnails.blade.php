<?php $sliderid = preg_replace('![^a-z0-9]+!i', '', get_sub_field('slider_id')); ?>
<?php if( have_rows('page_slider_images') ): 
  $i = 1; // Set the increment variable
  $t = 0; // Set the increment variable ?>

<div id="<?php echo $sliderid ?>" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">

      <?php while ( have_rows('page_slider_images') ) : the_row();?>
         <div class="carousel-item <?php if($i == 1) echo 'active';?> page_slider_images">
                <picture>
                  <source media="(max-width: 768px)" srcset="<?php the_sub_field('mob_image'); ?>" title="">
                  <img src="<?php the_sub_field('image'); ?>" class="slider-image" alt="<?php the_sub_field('slider_text'); ?>">
                </picture>
                <?php if( get_sub_field('slider_caption') ): ?>
                  <div class="colour-name"><?php the_sub_field('slider_caption'); ?></div>
                <?php endif; ?>
          </div>
        <?php   $i++; // Increment the increment variable
      endwhile; ?>
      
          </div>
       <a class="carousel-control-prev" href="#<?php echo $sliderid ?>" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#<?php echo $sliderid ?>" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        <ol class="carousel-indicators list-inline">
        <?php while ( have_rows('page_slider_images') ) : the_row();?>
         
                <li class="list-inline-item <?php if($t == 1) echo 'active';?>"> 
                  <a id="carousel-selector-<?php echo $t; ?>" class="selected" data-slide-to="<?php echo $t; ?>" data-target="#<?php echo $sliderid ?>">
                    <picture>
                      <source media="(max-width: 768px)" srcset="<?php the_sub_field('mob_image'); ?>" title="">
                      <img src="<?php the_sub_field('image'); ?>" class="slider-image" alt="<?php the_sub_field('slider_text'); ?>">
                    </picture>
                  </a>
                </li>
         
        <?php $t++; // Increment the increment variable
      endwhile; ?>
       </ol>
</div>


<?php
 else :

    // no rows found

endif;

?>
