


  <?php 
  $cat = get_sub_field('choose_wav');
  $news = new WP_Query(array(
      'posts_per_page'  => 4,
      'post_type'     => 'post',
      'category__not_in'  => $cat,
    )); ?>



   <?php if( $news->have_posts() ) : ?>
    <div id="testimonial" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">     
        <?php while( $news->have_posts() ) : ?> 
          <?php $news->the_post(); ?>
          <div class="carousel-item <?php if($i == 1) echo 'active';?> page_slider_images">
              <div class="row testimonal-slide">

                  <div class="col-sm-12 col-lg-5 post-featured-image align-self-center">
                    <img src="<?php the_post_thumbnail_url(); ?>" />
                  </div>
                  <div class="col-sm-12 col-lg-7 align-self-center testimonal-content">

                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="quote-left" class="svg-inline--fa fa-quote-left fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M464 256h-80v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8c-88.4 0-160 71.6-160 160v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48zm-288 0H96v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8C71.6 32 0 103.6 0 192v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48z"></path></svg>


                    @php the_excerpt() @endphp
                  <a href="{{ get_permalink() }}" class="button">Read Customer Story</a>


                   <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="quote-right" class="svg-inline--fa fa-quote-right fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M464 32H336c-26.5 0-48 21.5-48 48v128c0 26.5 21.5 48 48 48h80v64c0 35.3-28.7 64-64 64h-8c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24h8c88.4 0 160-71.6 160-160V80c0-26.5-21.5-48-48-48zm-288 0H48C21.5 32 0 53.5 0 80v128c0 26.5 21.5 48 48 48h80v64c0 35.3-28.7 64-64 64h-8c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24h8c88.4 0 160-71.6 160-160V80c0-26.5-21.5-48-48-48z"></path></svg>


                  </div>

                </div>
          </div>
        <?php   $i++; // Increment the increment variable
      endwhile; ?>
         </div>
       <a class="carousel-control-prev" href="#testimonial" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#testimonial" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>

    <?php endif; ?>
   <?php wp_reset_postdata(); ?>
