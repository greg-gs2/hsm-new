<?php if( get_field('footer_cta') ): ?>
  <?php if( get_field('footer_text') ): ?>
    <div class="container footer-cta">
      <div class="row">
        <div class="col-12">
          <?php the_field('footer_text'); ?> 
          <?php if( get_field('cta') == 'enquire_online'): ?>
              <button class="button" data-toggle="modal" data-target="#contactModal">Enquire Online</button>
          <?php endif; ?>
          <?php if( get_field('cta') == 'home_demo'): ?>
              <button class="button" data-toggle="modal" data-target="#homedemo">Home Demo</button>
          <?php endif; ?>
          <?php if( get_field('cta') == 'hire_quote'): ?>
              <button class="button" data-toggle="modal" data-target="#hiremodal">Hire Quote</button>
          <?php endif; ?>
          <?php if( get_field('cta') == 'test_drive'): ?>
              <button class="button" data-toggle="modal" data-target="#testdriveModal">Test Drive</button>
          <?php endif; ?>
          <?php if( get_field('cta') == 'button'): ?>
            <?php if( have_rows('button') ): ?>
               <?php while( have_rows('button') ) : the_row(); ?>
                    <button class="button" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></button>
                <?php endwhile; ?>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
<?php endif; ?>  