<article @php post_class() @endphp>
  <header>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    @include('partials/entry-meta')
  </header>
  <?php if( get_field('blog_featured_image') ): ?>
       <picture>
        <source media="(max-width: 630px)" srcset="<?php the_field('blog_featured_image_mobile'); ?>">
        <img src="<?php the_field('blog_featured_image'); ?>" alt="<?php the_title(); ?>" class="img-rounded blog-image">
      </picture>
  <?php else : ?>
      <?php if ( has_post_thumbnail()) : ?>
          <div class="col-lg-3 post-featured-image">
            <?php the_post_thumbnail(); ?>
          </div>
      <?php endif; ?>
  <?php endif; ?>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
</article>
