<?php $terms = get_the_terms($post->ID, 'category'); if( $terms ) : ?> 
	<time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time> | 
	<?php foreach( $terms as $term ) : ?><a href="<?= get_term_link($term, 'category'); ?>"><?= $term->name; ?></a><?php endforeach; ?><?php endif; ?>
