<?php

// Check value exists.
if( have_rows('page_content') ):

    // Loop through rows.
    while ( have_rows('page_content') ) : the_row(); ?>


      <!-- 2 Column Layout -->
        <?php if( get_row_layout() == '2_column_layout' ): ?> 

           <?php if( get_sub_field('expandable_readmore') ) : ?>
             <div class="container half-layout acf-cont readmore-button">
                  <div class="row d-lg-flex">
                    <a class="button" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Read More</a>
                  </div>
              </div>
          <?php endif?>


          <div <?php if( get_sub_field('expandable_readmore') ) : ?>id="collapse"<?php endif?> class="<?php if( get_sub_field('class') ): ?><?php the_sub_field('class'); ?> <?php endif; ?>container half-layout acf-cont <?php if( get_sub_field('expandable_readmore') ) : ?>collapse readmore-cont<?php endif?>"<?php if( get_sub_field('background_image') ) : ?>style="background-image: url(<?php the_sub_field('background_image') ?>); background-size: cover;<?php if( get_sub_field('parralex') ) : ?>background-attachment: fixed; background-position: center; background-repeat: no-repeat; background-size: cover;<?php endif?>"<?php  elseif( get_sub_field('background_colour') ): ?> style="background-color: <?php the_sub_field('background_colour'); ?>;"<?php endif?>>
            <div class="row d-lg-flex">
                       
                       <!-- Media Flexible content -->
                      <?php if( get_sub_field('media') ): ?>
                        <div class="col-lg-6">
                        <?php while( has_sub_field('media') ): ?>
                            <?php if( get_sub_field('text') ): ?>
                                <?php if( get_sub_field('read_more_text') ): ?><input id="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>" class="read-more-toggle" type="checkbox">
                                <div class="read-more-content"><div><?php endif; ?>
                                <?php the_sub_field('text'); ?>
                                <?php if( get_sub_field('read_more_text') ): ?></div></div>
                                <label class="read-more-toggle-label" for="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>">Read </label><?php endif; ?>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?>
                                @include('partials.builder-elements.page-slider')
                             <?php endif; ?>
                             <?php  if( get_row_layout() == 'page_slider_thumbnails' ): ?>
                                @include('partials.builder-elements.page-slider-thumbnails')
                             <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                                 <?php if( get_sub_field('image_link') ): ?><a href="<?php the_sub_field('image_link'); ?>"><?php endif; ?>
                                <img src="<?php the_sub_field('image'); ?>" />
                                <?php if( get_sub_field('image_link') ): ?></a><?php endif; ?>
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                              <div class="youtube-col"> 
                                <div class="embed-container">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                              </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <!-- END Media Flexible content -->

                    <!-- Media RIGHT Flexible content -->
                      <?php if( get_sub_field('media_right') ): ?>
                        <div class="col-lg-6"> 
                        <?php while( has_sub_field('media_right') ): ?>
                            <?php if( get_sub_field('text') ): ?>                              
                                <?php if( get_sub_field('read_more_text') ): ?><input id="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>" class="read-more-toggle" type="checkbox">
                                <div class="read-more-content"><div><?php endif; ?>
                                <?php the_sub_field('text'); ?>
                                <?php if( get_sub_field('read_more_text') ): ?></div></div>
                                <label class="read-more-toggle-label" for="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>">Read </label><?php endif; ?>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?>
                                @include('partials.builder-elements.page-slider')
                             <?php endif; ?>
                             <?php  if( get_row_layout() == 'page_slider_thumbnails' ): ?> 
                                @include('partials.builder-elements.page-slider-thumbnails')
                             <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                                <?php if( get_sub_field('image_link') ): ?><a href="<?php the_sub_field('image_link'); ?>"><?php endif; ?>
                                <img src="<?php the_sub_field('image'); ?>" />
                                <?php if( get_sub_field('image_link') ): ?></a><?php endif; ?>
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                              <div class="youtube-col"> 
                                <div class="embed-container">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                              </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <!-- END Media RIGHT Flexible content -->
                </div>
            </div> <!-- Flex -->
       <?php endif; ?>



       <!-- 3 Column Layout -->
        <?php if( get_row_layout() == '3_column_layout' ): ?> 

           <?php if( get_sub_field('expandable_readmore') ) : ?>
             <div class="container half-layout acf-cont readmore-button">
                  <div class="row d-lg-flex">
                    <a class="button" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Read More</a>
                  </div>
              </div>
          <?php endif?>


          <div <?php if( get_sub_field('expandable_readmore') ) : ?>id="collapse"<?php endif?> class="container half-layout acf-cont <?php if( get_sub_field('expandable_readmore') ) : ?>collapse readmore-cont<?php endif?>"<?php if( get_sub_field('background_image') ) : ?>style="background-image: url(<?php the_sub_field('background_image') ?>); background-size: cover;<?php if( get_sub_field('parralex') ) : ?>background-attachment: fixed; background-position: center; background-repeat: no-repeat; background-size: cover;<?php endif?>"<?php  elseif( get_sub_field('background_colour') ): ?> style="background-color: <?php the_sub_field('background_colour'); ?>;"<?php endif?>>
            <div class="row d-lg-flex">
                       
                      <!-- Media Flexible content -->
                      <?php if( get_sub_field('media') ): ?>
                        <div class="col-lg-4">
                        <?php while( has_sub_field('media') ): ?>
                            <?php if( get_sub_field('text') ): ?>
                                <?php if( get_sub_field('read_more_text') ): ?><input id="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>" class="read-more-toggle" type="checkbox">
                                <div class="read-more-content"><div><?php endif; ?>
                                <?php the_sub_field('text'); ?>
                                <?php if( get_sub_field('read_more_text') ): ?></div></div>
                                <label class="read-more-toggle-label" for="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>">Read </label><?php endif; ?>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?>
                                @include('partials.builder-elements.page-slider')
                             <?php endif; ?>
                             <?php  if( get_row_layout() == 'page_slider_thumbnails' ): ?>
                                @include('partials.builder-elements.page-slider-thumbnails')
                             <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                                <img src="<?php the_sub_field('image'); ?>" />
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                              <div class="youtube-col"> 
                                <div class="embed-container">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                              </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <!-- END Media Flexible content -->

                    <!-- Media CENTER Flexible content -->
                      <?php if( get_sub_field('media_center') ): ?>
                        <div class="col-lg-4"> 
                        <?php while( has_sub_field('media_center') ): ?>
                            <?php if( get_sub_field('text') ): ?>                              
                                <?php if( get_sub_field('read_more_text') ): ?><input id="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>" class="read-more-toggle" type="checkbox">
                                <div class="read-more-content"><div><?php endif; ?>
                                <?php the_sub_field('text'); ?>
                                <?php if( get_sub_field('read_more_text') ): ?></div></div>
                                <label class="read-more-toggle-label" for="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>">Read </label><?php endif; ?>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?>
                                @include('partials.builder-elements.page-slider')
                             <?php endif; ?>
                             <?php  if( get_row_layout() == 'page_slider_thumbnails' ): ?> 
                                @include('partials.builder-elements.page-slider-thumbnails')
                             <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                                <img src="<?php the_sub_field('image'); ?>" />
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                              <div class="youtube-col"> 
                                <div class="embed-container">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                              </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <!-- END Media CENTER Flexible content -->


                    <!-- Media RIGHT Flexible content -->
                      <?php if( get_sub_field('media_right') ): ?>
                        <div class="col-lg-4"> 
                        <?php while( has_sub_field('media_right') ): ?>
                            <?php if( get_sub_field('text') ): ?>                              
                                <?php if( get_sub_field('read_more_text') ): ?><input id="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>" class="read-more-toggle" type="checkbox">
                                <div class="read-more-content"><div><?php endif; ?>
                                <?php the_sub_field('text'); ?>
                                <?php if( get_sub_field('read_more_text') ): ?></div></div>
                                <label class="read-more-toggle-label" for="<?php echo strtolower(explode(' ', get_sub_field('text'), 2)[0]) ?>">Read </label><?php endif; ?>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?>
                                @include('partials.builder-elements.page-slider')
                             <?php endif; ?>
                             <?php  if( get_row_layout() == 'page_slider_thumbnails' ): ?> 
                                @include('partials.builder-elements.page-slider-thumbnails')
                             <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                                <img src="<?php the_sub_field('image'); ?>" />
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                              <div class="youtube-col"> 
                                <div class="embed-container">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                              </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <!-- END Media RIGHT Flexible content -->

                </div>
            </div> <!-- Flex -->

       <?php endif; ?>




       <!-- Full Width Layout -->
        <?php if( get_row_layout() == 'full_width_layout' ): ?> 
          <?php if( get_sub_field('media') ): ?>
              <?php while( has_sub_field('media') ): 
                $displayposts = get_sub_field('display_posts');
                $offset = get_sub_field('block'); ?>
              <?php endwhile; ?>
          <?php endif; ?>

          <?php if( get_sub_field('background_image') ) : ?><div style="background-image: url(<?php the_sub_field('background_image') ?>); background-size: cover;<?php if( get_sub_field('parralex') ) : ?> background-attachment: fixed; background-position: center; background-repeat: no-repeat; background-size: cover;<?php endif?>"<?php  elseif( get_sub_field('background_colour') ): ?> <div style="background-color: <?php the_sub_field('background_colour'); ?>;" ><?php endif; ?>

          <div class="<?php if( get_sub_field('background_image') ) : ?>all-white <?php endif; ?><?php if( get_sub_field('class') ): ?><?php the_sub_field('class'); ?> <?php endif; ?><?php if( get_sub_field('full_width') ) : ?>full-width <?php elseif ( $displayposts ): ?>full-width post-cont <?php endif?>acf-cont row<?php if ( $offset ): ?> offset<?php endif; ?>">
            <!-- <div class="row d-lg-flex">
              <div class="col-lg-12">  -->
                      <!-- Media Flexible content -->
                      <?php if( get_sub_field('media') ): ?>
                        <?php while( has_sub_field('media') ): ?>
                            <?php if( get_sub_field('text') ): ?>
                                <div class="col-12"> 
                                    <?php the_sub_field('text'); ?>
                                </div>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?> 
                                <div class="col-12"> 
                                    @include('partials.builder-elements.page-slider')
                                </div>
                             <?php endif; ?>
                             <?php  if( get_row_layout() == 'page_slider_thumbnails' ): ?> 
                                <div class="col-12"> 
                                    @include('partials.builder-elements.page-slider-thumbnails')
                                </div>
                             <?php endif; ?>
                             <?php if( get_sub_field('display_promo_boxes') ): ?>
                              @include('partials.builder-elements.promo-boxes')
                            <?php endif; ?>
                            <?php if( get_sub_field('display_posts') ): ?>
                              @include('partials.builder-elements.posts')
                            <?php endif; ?>
                            <?php if( get_sub_field('block') ): ?>
                              @include('partials.builder-elements.offset-blocks')
                            <?php endif; ?>
                            <?php if( get_sub_field('display_wavs') ): ?>
                              @include('partials.builder-elements.wavs')
                            <?php endif; ?>
                            <?php if( get_sub_field('display_lightweights') ): ?>
                              @include('partials.wav-elements.vehicle-range.lightweight')
                            <?php endif; ?>
                            <?php if( get_sub_field('display_9_seaters') ): ?>
                              @include('partials.wav-elements.vehicle-range.9_seaters')
                            <?php endif; ?>
                            <?php if( get_sub_field('display_17_seaters') ): ?>
                              @include('partials.wav-elements.vehicle-range.17_seaters')
                            <?php endif; ?>
                            <?php if( get_sub_field('display_customer_testimonials') ): ?>
                              @include('partials.builder-elements.testimonials')
                            <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                                <img src="<?php the_sub_field('image'); ?>" class="img-100"/>
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                                <div class="embed-container youtube-col">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- END Media Flexible content -->
                <!-- </div>
            </div> --> <!-- Flex -->
        </div>  

        <?php if( get_sub_field('background_image') ) : ?>
                <?php  elseif( get_sub_field('background_colour') ): ?>
                </div>
            <?php endif?>

       <?php endif; ?>


       <!-- Tabs -->
       <?php if( get_row_layout() == 'collapse_tabs' ): ?> 
              <div class="container acf-cont page-accordion">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <h2 class="accordion-maintitle"><?php the_sub_field('title'); ?></h2>
                </div>
                  @include('partials.builder-elements.tabs')
                
              </div>
            </div>
       <?php endif; ?>


       <?php 
       // Case: Quote Layout.
        if( get_row_layout() == 'three_column_text' ): ?> 
              <div class="container acf-cont">
                <div class="row">
                  <div class="col-lg-12"><h2><?php the_sub_field('col_heading'); ?></h2></div>
                  <div class="col-lg-12 three-col-text"> <?php the_sub_field('text'); ?> </div>
                </div>
              </div>
       <?php endif; ?>



       

     <?php // End loop.
    endwhile;

// No value.
else : ?>
   <div class="entry-content">
      <?php the_content(); ?>
    </div>
 <?php endif; ?> 