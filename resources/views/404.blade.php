@extends('layouts.app')

@section('content')
  
<div class="container">
  <div class="row">
    <div class="col-12">
      <?php if( get_field('404_page_intro', 'options') ): ?>
        <?php the_field('404_page_intro', 'options'); ?>
      <?php endif; ?>
    </div>
    @if (!have_posts())
    <div class="col-12 404-search">
      {!! get_search_form(false) !!}
    </div>
    @endif
      <div class="col-12">
        <?php if( get_field('404_page_image_link', 'options') ): ?><a href="<?php the_field('404_page_image_link', 'options'); ?>"><?php endif; ?>
        <?php if( get_field('404_page_image', 'options') ): ?>
        <img src="<?php the_field('404_page_image', 'options'); ?>" style="width: 100%;"/>
      <?php endif; ?>
      <?php if( get_field('404_page_image_link', 'options') ): ?></a><?php endif; ?>
    </div>
  </div>

  <?php if( get_field('404_wav_heading', 'options') ): ?>
    <div class="row">
      <h1><?php the_field('404_wav_heading', 'options'); ?></h1>
      @include('partials.builder-elements.wavs')
    </div>
  <?php endif; ?>
</div>
@endsection
