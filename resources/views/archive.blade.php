@extends('layouts.app')

@section('content')
  {{--@include('partials.page-header')--}}

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

   <div class="row">
    <div class="col-sm-12 col-lg-9">
      <div class="page-header">
        <h1>{!! single_term_title() !!}</h1>
      </div>

      <?php $cat_ID = get_query_var('cat');
            $featured = new WP_Query(array(
            'posts_per_page'  => 1,
            'offset' => 0,
            'post_type'     => 'post',
            'category__in'    => $cat_ID
          )); ?>

          @include('partials.builder-elements.news.featured')

      @while (have_posts()) @php the_post() @endphp
          @include('partials.content-'.get_post_type()) 
      @endwhile
    </div>
    <div class="col-sm-12 col-lg-3 sidebar">
      @include('partials.sidebar')
      Hello
    </div>
  </div>


  {{--{!! get_the_posts_navigation() !!}--}}
  <?php numeric_posts_nav(); ?>
@endsection
