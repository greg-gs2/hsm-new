@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-12">
      @include('partials.page-header')
    </div>
  </div>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

<div class="search-cont">
  <div class="row">
    @while(have_posts()) @php the_post() @endphp
      @include('partials.content-search')
    @endwhile
  </div>
</div>

  {{--{!! get_the_posts_navigation() !!}--}}
  <div class="row">
    <div class="col-12">
      <?php numeric_posts_nav(); ?>
    </div>
  </div>
@endsection
