@extends('layouts.app')

@section('content')
<div class="row">
  @while(have_posts()) @php the_post() @endphp 
    <div class="col-sm-12 col-lg-9">
      @include('partials.content-single-'.get_post_type())
    </div>
    <div class="col-sm-12 col-lg-3 sidebar sidebar-promos">
        @include('partials.builder-elements.promo-boxes')
        @include('partials.sidebar')
    </div>
  @endwhile
</div>
@endsection
